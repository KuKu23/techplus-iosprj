//
//  HomeViewController.swift
//  tabledemo2TrainSchool
//
//  Created by Min Aung Hein on 7/20/19.
//  Copyright © 2019 Min Aung Hein. All rights reserved.
//


import UIKit

class HomeViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView:UITableView!
    
    var courses = [
                                            [ "John","Macaw","Susi","Dean","Chroly"      ] ,
                                            [ "Albert","Susan","Julier","Alan","Arrial","Robert"    ],
                                            [ "Mi Mi","Crteal","Alex","Albert","Susan","Julier","Alan","Arrial","Robert" ,"Marri"   ],
                                            [ "Elli","Mohamad","Cristal"   ]
                                ]
    var courseTitle = [ "iOS Developer Class","Andriod Developer Class","Web Developer Course","Graphic Design"     ]
    //var course = [[String ]]()  ,
    //Course 4 ,
    //1 -> 5
    //2 - > 6
    //3 - > 10
    //4 - > 3
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  courses.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        print(indexPath.section, indexPath.row )
        cell.textLabel?.text = courses[indexPath.section][indexPath.row] // courses[ 0 ] [ 1 ]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailsegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return courseTitle[section]
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Total Student:\(courses[section].count )"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

}
