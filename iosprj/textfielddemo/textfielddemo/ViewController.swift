//
//  ViewController.swift
//  textfielddemo
//
//  Created by Khu Khu on 7/6/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {
    //1.Adapt Protocol
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    
    //Gesture
    
    @IBAction func tapTheView(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func hideKB(_ sender: UIButton) {
       
        textField1.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //2.get delegation from element
        textField1.delegate = self
        textField2.delegate = self
        
        //self,this class' obj is assign to textfield's delegate,features
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textField1{
            textField2.becomeFirstResponder()//hey bring kb
        }else{
            textField.resignFirstResponder()
        }
        return true
    }


}

