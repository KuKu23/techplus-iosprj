//func substract(num1:Double,num2:Double)->Double{
//    let r = num1 - num2
//    return r
//}
//
//func multiply(_ num1:Double,_ num2:Double)->Double{
//    return num1*num2
//}
//
//func divide(_ num1:Double,_ num2:Double)->Double{
//    return num1/num2
//}
//
////func overload
//func add(num1:Double,num2:Double,num3:Double)->Double{
//    return num1 + num2 + num3
//}
//
//let r2 = substract(num1: 5.0, num2: 2.0)
//let rO = add(num1: 1.0, num2: 2.0, num3: 3.0)
//let r3 = multiply(6,7.0)
//let _ = multiply(7.0,8.0)
//let r4 = divide(25, 5.0)
//print(r2)
//
//func calculateWeight(m mass:Double,g gravity:Double)->Double{
//    return mass*gravity
//}
//
//let w = calculateWeight(m:10,g:9.8)
//print("Weight:",w)
//
////pythagoras theorem
////c = squareRoot of a square + b square
//func calculateLongestDist(a:Double,b:Double)->Double{
//    let r = (a*a + b*b).squareRoot()
//    return r
//}
//
////Distance between point
//func getDistance(x1:Double,y1:Double,x2:Double,y2:Double)->Double{
//    let deltaX = x2-x1
//    let deltaY = y2-y1
//    return (deltaX*deltaX + deltaY*deltaY).squareRoot()
//}
//
////v1 = v0 + at
//func getFinalVelocity(initialVelocity v0:Double,acceleration a:Double,time t:Double)->Double{
//    return v0 + a*t
//}
//
////5.function as a variable,First class citizen
//
//func calculate(){
//
//}
//
//let a = calculate
//
//var b:()->()?
//b = calculate

//func func1(num1:Double)->Double{
//    return 0
//}
////b = func1
//var c:(Double) ->(Double)
//c = func1

var myFunctions = [ (Int)->()    ]()

func funcA(n1:Int){}
func funcB(n2:Int){}

myFunctions.append (funcA)
myFunctions.append (funcB)

let g = myFunctions[0]
g(1)

func calculate(num1:Int,num2:Int,process:(Int,Int)->(Int)){
    let r = process(num1,num2)
    print("==>",r)
}

func add(num1:Int,num2:Int)->Int{
    return num1+num2
}

func substract(num1:Int,num2:Int)->Int{
    return num1-num2
}
calculate(num1:2,num2:3,process:add)
calculate(num1:2,num2:3,process:substract)
//closure/event callback
calculate(num1: 2, num2: 3, process: {(a,b) in
    return a*b
    
})

calculate(num1: 25, num2: 5) { (a, b) -> (Int) in
    return a/b
}

calculate(num1: 3, num2: 6){(a,b)->Int  in
    return a*b
}


func averageVari(_ num:Double...)->Double{
    print(num)
    var total = 0.0
    for n in num{
        total += n
    }
    return total/Double(num.count)
}

func average(_ num1:Double,_ num2:Double)->Double{
    return (num1 + num2)/2
}

let ra = average(12,8)
let rva = averageVari(3,4,5,6)
print(ra,rva)

var x = 10
var y = 20

func iswap(_ n1:inout Int,_ n2:inout Int){
    let v1 = n1
    n1=n2
    n2=v1
}

iswap(&x, &y)
print("X:",x,"y:",y)

