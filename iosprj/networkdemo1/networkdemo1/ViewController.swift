//
//  ViewController.swift
//  networkdemo1
//
//  Created by Min Aung Hein on 11/2/19.
//  Copyright © 2019 Min Aung Hein. All rights reserved.
//

import UIKit

struct ExchangeRateInfo:Codable {
    var description:String
    var timestamp:String
    var info:String
    var rates:[String:String]
}



class ViewController: UIViewController {
    
    
    @IBOutlet weak var rateLabel: UILabel!
    
    
    let baseURLStr = "https://forex.cbm.gov.mm"
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    
    @IBAction func loadExchange(_ sender: UIButton) {
        
        let latestURLStr =  baseURLStr + "/api/latest"
        if let latestURL = URL(string: latestURLStr) {
        
            //URLSession + URLRequest
            let myConfig = URLSessionConfiguration.default
            var session = URLSession(configuration: myConfig)
            var request = URLRequest(url: latestURL)
            request.allHTTPHeaderFields = [
                "Content-Type":"application/x-www-form-urlencoded",
                "accept":"application/json"
            ]

            let task = session.dataTask(with: request) { (data, response, error) in
                
                //print(data)
                //print(response?.mimeType)
                //print(error)
                if let error = error {
                    print(error.localizedDescription)
                } else if let data = data {
                    
                    let decoder = JSONDecoder()
                    let exchangeRateInfo = try! decoder.decode(ExchangeRateInfo.self, from: data)
                    print(exchangeRateInfo.description)
                    print(exchangeRateInfo.rates["SGD"])
                    
                    
     
                } else {
                    print("No Data")
                }
                
            }
            task.resume()
   
        }
    }
}

