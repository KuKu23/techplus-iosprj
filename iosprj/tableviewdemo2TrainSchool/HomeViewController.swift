//
//  HomeViewController.swift
//  tableviewdemo2TrainSchool
//
//  Created by Khu Khu on 7/20/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView:UITableView!
    
    
    
    var courses = [ ["John","Macaw","Susi","Dean","Chroly"]
        ,["Albert","Susan","Julier","Alan","Robert"]
        ,["Mi Mi","Crteal","Alex","Albert","Susan","Julier","Alan","Robert","John","Macaw",]
        ,["Elli","Mohamad","Cristal"]
        
                ]
    
    var courseTitle = ["iOS Developer Class","Android Developer Class","Web Developer Class","Graphic Design"]
    //var course = [[String]
    //Course 4,
    //1->5
    //2->6
    //3->10
    //4->3
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return courses.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        print(indexPath.section,indexPath.row)
        cell.textLabel?.text = courses[indexPath.section][indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailsegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return courseTitle[section]
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Total Student:\(courses[section].count)"
    }
    
   
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
