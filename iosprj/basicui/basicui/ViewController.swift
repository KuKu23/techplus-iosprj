//
//  ViewController.swift
//  basicui
//
//  Created by Khu Khu on 6/8/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let width = view.frame.size.width
        let height = view.frame.size.height
        var offset:CGFloat = 0.0
        let leftMargin:CGFloat = 20
        let vGap:CGFloat = 20
        
        let bgView = UIView()
        bgView.backgroundColor = UIColor.init(displayP3Red: 100/255, green: 100/255, blue: 1, alpha: 0.5)
        bgView.frame = CGRect(x:0,y:0,width:width,height:height)
        self.view.addSubview(bgView)
        
        //label
        let titleLabel = UILabel()
        titleLabel.text = " iOS UI Demo"
        titleLabel.font = UIFont.boldSystemFont(ofSize: 30)
        titleLabel.textAlignment = .center
        titleLabel.frame = CGRect(x:0,y:50,width:width,height:titleLabel.intrinsicContentSize.height)
        titleLabel.textColor = UIColor.white
        view.addSubview(titleLabel)
        offset = titleLabel.frame.origin.y + titleLabel.frame.size.height
        
        //button
       let btn1 = UIButton()
       btn1.setTitle("B1", for: .normal)
       btn1.backgroundColor = UIColor.white
        btn1.setTitleColor(UIColor.black, for: .normal)
       btn1.frame = CGRect(x: leftMargin, y: offset + vGap, width: width/2 - leftMargin - vGap/2, height: height/6)
        btn1.layer.cornerRadius = 7
        view.addSubview(btn1)
        
        let btn2 = UIButton()
        btn2.setTitle("B2", for: .normal)
        btn2.backgroundColor = UIColor.white
        btn2.setTitleColor(UIColor.black, for: .normal)
        btn2.frame = CGRect(x: btn1.frame.origin.x + btn1.frame.size.width + vGap, y:btn1.frame.origin.y, width: width/2 - leftMargin - vGap/2, height: height/6)
        btn2.layer.cornerRadius = 7
        view.addSubview(btn2)
        offset = btn2.frame.origin.y + btn2.frame.size.height
        
        //switch
        let sw1 = UISwitch()
        sw1.frame = CGRect(x: leftMargin, y: offset + vGap, width: 200, height: 200)
        sw1.isOn = true
        view.addSubview(sw1)
        offset = sw1.frame.origin.y + sw1.frame.size.height
        
        //textfield
        let textField = UITextField()
        textField.frame = CGRect(x: leftMargin, y: offset + vGap, width: width - vGap * 2, height:textField.intrinsicContentSize.height + 10)
        textField.backgroundColor = UIColor.white
        textField.placeholder = "Enter PIN number"
        textField.textAlignment = .center
        view.addSubview(textField)
        offset = textField.frame.origin.y + textField.frame.size.height
        
        //textview
        let textView = UITextView()
        textView.frame = CGRect(x: leftMargin, y: offset + vGap, width: width - vGap * 2, height:50)
        textView.backgroundColor = UIColor.white
        textView.isEditable = false
        view.addSubview(textView)
        offset = textView.frame.origin.y + textView.frame.size.height
        
        //imageview
        let imageView = UIImageView()
        imageView.frame =  CGRect(x: leftMargin, y: offset + vGap, width: width - vGap * 2, height:50)
        view.addSubview(imageView)
        imageView.image = UIImage(named: "photo1.jpg")
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        offset = imageView.frame.origin.y + imageView.frame.size.height
        
        //stepper
        let stepper = UIStepper()
        stepper.center = CGPoint(x:leftMargin + stepper.intrinsicContentSize.width/2,y:offset + vGap + stepper.intrinsicContentSize.height/2)
        stepper.maximumValue = 20
        view.addSubview(stepper)
        offset = stepper.frame.origin.y + stepper.frame.size.height
        
        //segment
        let segment = UISegmentedControl(items:["Opt1","Opt2","Opt3"])
        segment.frame = CGRect(x: leftMargin, y: offset + vGap, width: width - vGap * 2, height:50)
        segment.insertSegment(withTitle: "Opt1.5", at: 1, animated: false)
        view.addSubview(segment)
        offset = segment.frame.origin.y + segment.frame.size.height
        
        
        //slider
        let slider = UISlider()
        slider.frame = CGRect(x: leftMargin, y: offset + vGap, width: width - vGap * 2, height:100)
        slider.maximumValue = 100
        slider.minimumValue = 25
        slider.value = 52
        view.addSubview(slider)
        offset = slider.frame.origin.y + slider.frame.size.height
       
        
        //progress
        let progressView = UIProgressView()
        progressView.frame = CGRect(x: leftMargin, y: offset + vGap, width: width - vGap * 2, height:100)
        progressView.progress = 0.20
        progressView.progressTintColor = UIColor.red
        view.addSubview(progressView)
        offset = progressView.frame.origin.y + progressView.frame.size.height
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.frame = CGRect(x: leftMargin, y: offset + vGap, width: width - vGap * 2, height:60)
        let barBtn = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: nil)
        let pausebarBtn =  UIBarButtonItem(barButtonSystemItem: .pause, target: self, action: nil)
        let flexBtn =  UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolbar.items = [barBtn,flexBtn,pausebarBtn]
        view.addSubview(toolbar)
        
        
        
    }


}

