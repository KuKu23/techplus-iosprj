import UIKit

class Fish{
    private var _id:Int
    var color:UIColor
    var id:Int{
        return _id
    }
    private var speed:Float = 0
    private var age:Float = 0
    private var lifeSpan:Float
    var visibility:Float = 100
    var status:Int//0-sleep,1-swimming forward,2-rotate left,3-rotate right,8-Hungry,16-----
    var body:UIView
    
    init(id:Int) {
        self._id = id
        self.color = UIColor.white
        self.age = 1//in day
        self.status = 0
        self.body = UIView()
        self.visibility = 100
        self.lifeSpan = 30000//30sec
    }
    
    func getAge()->Float{//Getter
        return self.age
    }
    
    func swim(){
        
    }
    func swim(x:CGFloat,y:CGFloat){
        
    }
    func swim(dx:CGFloat,dy:CGFloat){
        
    }
    func findFood(){
        
    }
}
