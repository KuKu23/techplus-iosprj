//
//  ViewController.swift
//  alertme
//
//  Created by Min Aung Hein on 8/17/19.
//  Copyright © 2019 Min Aung Hein. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var counter = 0
    @IBOutlet weak var counterLabel: UILabel!
    
    func updateCounter( ) {
        counterLabel.text =  String(counter)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateCounter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
       // showAlert()
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGesture)
        tapGesture.addTarget(self, action: #selector(tap))
    }
    
    var oldDegree:CGFloat = 0
    let thrDegree:CGFloat = 15
    
    @IBAction func rotate(_ sender: UIRotationGestureRecognizer) {
        
        if sender.state  ==   UIGestureRecognizer.State.began {
            oldDegree = 0
        }
        let currentDeg =  sender.rotation * 180.0 / CGFloat(Double.pi)
       
        if  ((currentDeg - oldDegree) > thrDegree ) {
        counter += 5
            oldDegree =  currentDeg
        updateCounter()
        } else if ((oldDegree - currentDeg) > thrDegree ) {
            guard counter > 0 else { return }
            counter -= 5
            oldDegree =  currentDeg
            updateCounter()
        }
    }
    var oldRatio:CGFloat = 0
    @IBAction func pinch(_ sender: UIPinchGestureRecognizer) {
        let ratio = sender.scale
        print(ratio)
        if sender.state == UIGestureRecognizer.State.began {
            oldRatio = 0 
        }
        if ( ratio - oldRatio ) > 1.2 {
        counter +=  50
            oldRatio = ratio
        updateCounter()
        }
    }
    
    @IBAction func longPress(_ sender: UILongPressGestureRecognizer) {
        
        counter = 0
        updateCounter()
    }
    
   @objc func tap(_ gesture:UITapGestureRecognizer ){
       counter += 1
      updateCounter()
    }

    @IBAction func swipe(_ sender:
        UISwipeGestureRecognizer) {
        guard counter > 0 else { return }
        counter -= 1
        updateCounter()
    }
    
    func showAlert() {
        
        let alertCtrl = UIAlertController(title: "Going Trip", message: "Your upcoming vacation plan might be ", preferredStyle: .actionSheet)
        let delAction = UIAlertAction(title: "Delete", style: .destructive, handler: doIt)
        
        let okAction =  UIAlertAction(title: "Ok", style: .default, handler: doIt)
        let thinkAction = UIAlertAction(title: "Think", style: .cancel, handler: doIt)
        
        alertCtrl.addAction(delAction)
        alertCtrl.addAction(okAction)
        alertCtrl.addAction(okAction)
        alertCtrl.addAction(okAction)
        alertCtrl.addAction(okAction)
        alertCtrl.addAction(okAction)
        
        alertCtrl.addAction(thinkAction)
        
        present(alertCtrl, animated: true, completion: nil)
    }
    func doIt(_ action:UIAlertAction){
        switch action.title {
        case "Delete": print("Deleting")
        case "Ok":print("OK")
        case "Think":print("Thinking")
        default:print("Do nothing")
        }
    }
    
}

