//
//  menuViewController.swift
//  coffeeshop
//
//  Created by Khu Khu on 7/15/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class menuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    let coffetypes = ["Expresso","Capuccino","Macciato","Macha","Latte"]
    let cupImageStrings = ["cup1.png","cup2.png","cup3.png","cup4.png","cup5.png"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.size.height/6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //prototype cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! coffeeTableViewCell//type casting
        //coz of class assignment already
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        cell.coffeeLabel.text = coffetypes[indexPath.row]
        cell.cupImageView.image = UIImage(named: cupImageStrings[indexPath.row])
        
        
        return cell
    }
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
