//
//  coffeeTableViewCell.swift
//  coffeeshop
//
//  Created by Khu Khu on 7/15/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class coffeeTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var cupImageView: UIImageView!
    
    
    
    @IBOutlet weak var coffeeLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
