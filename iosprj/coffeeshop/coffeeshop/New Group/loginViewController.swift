//
//  loginViewController.swift
//  coffeeshop
//
//  Created by Khu Khu on 7/11/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class loginViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var textfield1: UITextField!
    
    
    @IBOutlet weak var textfield2: UITextField!
    
    
    @IBAction func tapTheView(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfield1.delegate = self
        textfield2.delegate = self
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textfield1{
            textfield2.becomeFirstResponder()//hey bring kb
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
