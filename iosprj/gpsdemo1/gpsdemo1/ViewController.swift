//
//  ViewController.swift
//  gpsdemo1
//
//  Created by Khu Khu on 9/14/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var map:MKMapView!
    
    let clm = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        promptForLocation(UIButton())
        map
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func locationManager(_manager:CLLocationManager,didUpdateLocations locations:[CLLocation]){
        print(locations.first)
    }
    
    func promptToGoSetting(){
        let urlString = UIApplication.openSettingsURLString
        if let url = URL(string: urlString){UIApplication.shared.open(url, options: [:]){(staus) in print("setting opened")//go to setting
        }
    }
        


}
    func setupCoreLocatinManager(){
        clm.desiredAccuracy = kCLLocationAccuracyBest
        clm.pausesLocationUpdatesAutomatically = false
        //clm.allowsBackgroundLocationUpdates = true
        clm.delegate = self
        clm.startUpdatingLocation()
        
    }
    
   
    
    @IBAction func promptForLocation(_ sender: UIButton) {
        let status = CLLocationManager.authorizationStatus()
        
        switch(status){
        case.authorizedAlways,.authorizedWhenInUse
            :print("user granted!!")
            setupCoreLocatinManager()
            
        case.denied
            :print("user denied:(")
            promptToGoSetting()
            
        case.notDetermined:print("Not determined,good change to ask for permission")
        self.clm.requestWhenInUseAuthorization()
            
        default:break;
        }
    }
    
}
