//
//  HomeViewController.swift
//  navigator
//
//  Created by Khu Khu on 6/29/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBAction func showInfo(_ sender: UIButton) {
        performSegue(withIdentifier: "infosegue", sender: nil)
    }
    
    @IBAction func showdeatil(_ sender: UIButton)
    {
        performSegue(withIdentifier: "detailsegue", sender: nil)
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
