class Car{
    var id:Int
    var numWheel:Int = 4
    private var color:String = "White"
    internal var tintColor:String = "White"
    
    
    init(id:Int) {
        self.id = id
    }
}

class Jeep:Car{
    func paint(){
        self.tintColor = "Green"
    }
}

var car1 = Car(id: 1)

var jeep1 = Jeep(id: 2)
