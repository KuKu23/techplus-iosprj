
var countdown = 10
while countdown >= 0 {
    print (countdown)
    if countdown == 4{
        print("I'm bored.Let's go now!")
        break
    }
    countdown -= 1
}

for i in 1...10{
    for j in 1...10{
        let product = i*j
        print("\(i) * \(j) is \(product)")
    }
}

func greet(_ person:String,nicely:Bool = true){
    if nicely == true{
        print("Hello, \(person)!")
    }else{
        print("Oh no, it's \(person) agian...")
    }
}
greet("Taylor")
greet("Taylor",nicely: false)


func square(numbers:Int...){
    for number in numbers {
        print("\(number) squared is \(number * number)")
    }
}
square(numbers: 1,2,3,4,5)
