//
//  ViewController.swift
//  constrainbycodedemo2
//
//  Created by Khu Khu on 7/21/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    @IBAction func additem(_ sender: UIBarButtonItem) {
        let btn = UIButton()
        addConstraint(childView: btn, parentView: view)
        
    }
    
    func addConstraint(childView:UIView,parentView:UIView){
       
        
        let topMargin:CGFloat = parentView.subviews.count == 0 ?80:20
        
        let refView = parentView.subviews.count == 0 ? parentView:parentView.subviews.last!
        
        parentView.addSubview(childView)
        childView.backgroundColor = UIColor.green
    childView.translatesAutoresizingMaskIntoConstraints = false
       
        if parentView.subviews.count == 1{
            childView.topAnchor.constraint(equalTo: refView.topAnchor, constant: 20).isActive = true
        }else{
            childView.topAnchor.constraint(equalTo: refView.bottomAnchor, constant: 20).isActive = true
        }
        
        childView.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 20).isActive = true
        childView.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: -20).isActive = true
        childView.heightAnchor.constraint(equalTo: parentView.heightAnchor, multiplier: 0.20).isActive = true
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

