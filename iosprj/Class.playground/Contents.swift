import UIKit

class NamedShape{
    var numberOfSides:Int = 0
    var name:String
    init(name:String) {
        self.name = name
        //print("NameShape is initialized")
    }
    func simpleDescriptionn() -> String {
        
        return "A Shape with \(numberOfSides) sides"
    }
}


//print(shape.simpleDescriptionn())


class Square:NamedShape{
    
    var sideThickness = 0
    override init(name: String) {
        sideThickness = 1
        super.init(name: name)
        self.numberOfSides = 4
    }
    override func simpleDescriptionn() -> String {
           return "A Shape with \(numberOfSides) sides -> \(name)"
       }
    
    func move(){
        print("Square is moving")
    }
    
}


//print(square1.simpleDescriptionn())

class Triangle:NamedShape{
    override init(name: String) {
        super.init(name: name)
        self.numberOfSides = 3
    }
    override func simpleDescriptionn() -> String {
        super.simpleDescriptionn()
        return "A Shape with \(numberOfSides) sides -> \(name)"
    }
    
}
var shape = NamedShape(name: "Unknow Shape")
var square1 = Square(name: "Square")
var triangle = Triangle(name: "Triangle")
//print(triangle.simpleDescriptionn())


var  nameShapes = [NamedShape]()
nameShapes.append(shape)
nameShapes.append(square1)
nameShapes.append(triangle)

for shape in nameShapes{
    
    (shape as? Square)?.move()
    
    if(shape is Square) {
        print("This is Nameshape")
       // print((shape as! Square).sideThickness)
    }
    print(shape.simpleDescriptionn())
}

func decrease(number:Int){
    print(number)
    if number > 0 {
    decrease(number: number - 1)
    }
}

decrease(number: 20)

//1.
let firstnum = 15
let secondnum = 20

let max = firstnum > secondnum ? firstnum:secondnum



//2.
func getMaxAverage(nums:Double ...)->(Double,Double)?{
    
    if(nums.count > 0){
        var maxNum = nums.first ?? 0
        var sum = 0.0
        
        for n in nums{
            maxNum =  n > maxNum ? n : maxNum
            sum += n
        }
        return ( maxNum,sum / Double(nums.count))
    }
    return nil
    
}

var result = getMaxAverage(nums: 10,20,30,40,50)
print(result?.0)





//3.
var isEven:Bool{return 12%2 == 0}

var isOdd:Bool {return 14%2 != 0}


protocol EmergencyProtocol{
    
    var firstAidKit:Int{ get set }
    var id:Int {get set}
    init(id:Int)
    
    func moveToSaftyZone()
    
    func keepExplosiveMateApartFromFire()
    
    func keepToPrioritymaterial()
}


class Office:EmergencyProtocol{
    var id:Int
    
    required init(id: Int) {
        self.id = id
        self.numPeople = 0
        self.firstAidKit = 0
        self.name = ""
    }
    
    var firstAidKit: Int
    
   
    
    var numPeople:Int
    var name:String
    
    init(numPeople:Int,name:String) {
        self.name = name
        self.numPeople =  numPeople
        self.firstAidKit = 5
        self.id = 5
        
    }
    func moveToSaftyZone() {
           <#code#>
       }
       
       func keepExplosiveMateApartFromFire() {
           <#code#>
       }
       
       func keepToPrioritymaterial() {
           <#code#>
       }
}

var myOffice = Office(numPeople: 100, name: "HQ")
//myOffice.moveToSaftyZone()
myOffice.firstAidKit

class Ship{
    
    
    var mycreticaloperation:EmergencyProtocol?
    
    
    
}

class Dice{
    var number = 1
}

protocol ThreeDiceGame:DiceGame{
    
}


protocol DiceGame:AnyObject{
    var dice:Dice{ get }
    func play()
}

protocol DiceGameDelegate{
    func gameDidStart()
    func gameDidEnd()
}




class SnakenLadder:DiceGame{
    var dice: Dice
    
    func play() {
        delegate?.gameDidStart()
        delegate?.gameDidEnd()
    }
    
    init() {
        dice = Dice()
        dice.number = 1
    }
    
    var delegate:DiceGameDelegate?
    
    
}



//subscript

class MyDataManager{
    var dataID = 3
    
    var name = ""
    
    subscript (index:Int) -> Int{
        set{
            //code here what to when data is in
           dataID *= index
        }
        get{
            //code here what to when data is request as per index
            return dataID
        }
    }
}

var data1 = MyDataManager()

print(data1.dataID)
data1[3] = 5
print("Subscript")
print(data1.dataID)



