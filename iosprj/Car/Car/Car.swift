//
//  Car.swift
//  Car
//
//  Created by Khu Khu on 8/17/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import Foundation
//private
//internal
//fileprivate
//override-easy to override method
//override- hard/No to override properties but ok to computed properties
//static-share instance ONLY to class
 class Car{
   static var numCars:Int = 0//setting static to var
    //make it Share instance
    static var cars = [Car]()
    
    var id:Int
    var numWheel:Int = 4
    var coverWheel:Int{
        return numWheel
    }
    private var color:String = "White"
    fileprivate var EFI:String = ""
    internal var tintColor:String = "White"
    
    
    init(id:Int) {
        self.id = id
        Car.numCars += 1
        Car.cars.append(self)
    }
   static func driveAll() {
    for car in Car.cars{
        car.drive()
    }
    }
    
    func drive(){
        print("Drive normally")
    }
}

class Truck:Car{
   override var coverWheel: Int{
        return numWheel*2
    }
    override init(id:Int){
        super.init(id: id)//must be called
    }
    
    func setAutoDrive(){
        self.EFI = "V2019EFI"
    }
    override func drive() {
        //
        super.drive()
        print("Drive safely")
    }
}




