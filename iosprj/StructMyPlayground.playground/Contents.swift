import UIKit
//deep copy
//can't inheritance
struct Person{
    var first_name:String
    var last_name:String
    var country:String
    
    func displayName()  {
        print(first_name + last_name)
    }

}

var p1 = Person(first_name: "Tim", last_name: "Jay", country: "USA")


let jsonString = """
{
    "first_name": "John",
    "last_name": "Doe",
    "country": "United Kingdom"
}
"""
var data = jsonString.data(using: .utf8)!

struct User:Codable {//1*
    var firstName:String
    var lastName:String
    var country:String
    
    enum CodingKeys:String, CodingKey{// Assignment declaration for mismatch JSON key
        case firstName = "first_name"
        case lastName = "last_name"
        case country
    }
}

var decoder = JSONDecoder()
do{
    var user = try decoder.decode(User.self, from: data)
    print(user.firstName)
    print(user.lastName)
} catch let error {
    print(error.localizedDescription)
}

