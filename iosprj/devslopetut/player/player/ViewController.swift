//
//  ViewController.swift
//  player
//
//  Created by Khu Khu on 8/18/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit
import AVKit

class ViewController: UIViewController {

    var player:AVAudioPlayer!
    var videoplayer:AVPlayer!
    
    
    @IBAction func stop(_ sender: UIBarButtonItem) {
        player.stop()
    }
    
    
    @IBAction func play(_ sender: UIBarButtonItem) {
        player.play()
    }
    
    
    @IBAction func pause(_ sender: UIBarButtonItem) {
        player.pause()
    }
    
    
    @IBAction func playvideo(_ sender: UIButton) {
        let videoPlayerController = AVPlayerViewController()
        let videofileURL = Bundle.main.url(forResource: "butterfly", withExtension: "mp4")!
        videoplayer = AVPlayer(url:videofileURL)
        videoPlayerController.player = videoplayer
        videoPlayerController.view.frame = CGRect(x: 20, y: 20, width: 100, height: 100)
        //present(videoPlayerController,animated: true,completion: nil)
        view.addSubview(videoPlayerController.view)
        videoPlayerController.player?.play()
    }
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let fileURL = Bundle.main.url(forResource: "Img 2800", withExtension: "mp3"){
            do{
                player = try AVAudioPlayer(contentsOf: fileURL)
                player.play()
            }catch let error{
            print(error)
        }
    }


}
}
