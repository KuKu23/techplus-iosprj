//
//  ViewController.swift
//  MeterBillCalculater
//
//  Created by Thet Tun Oo on 6/29/19.
//  Copyright © 2019 Thet Tun Oo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var meterTextField: UITextField!
    
    @IBOutlet weak var onetothirtyLabel: UILabel!
    @IBOutlet weak var thirtyonetofiftyLabel: UILabel!
    @IBOutlet weak var fiftyonetoseventyfiveLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var seventysixtohundredLabel: UILabel!
    @IBOutlet weak var hundredonetohundredfiftyLabel: UILabel!
    @IBOutlet weak var hundredfiftyonetotwohundredLabel: UILabel!
    @IBOutlet weak var uptoTwohundredoneLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func changeMeter(_ sender: Any) {
        onetothirtyLabel.text = ":"
        thirtyonetofiftyLabel.text = ":"
        fiftyonetoseventyfiveLabel.text =  ":"
        seventysixtohundredLabel.text = ":"
        hundredonetohundredfiftyLabel.text = ":"
        hundredfiftyonetotwohundredLabel.text = ":"
        uptoTwohundredoneLabel.text = ":"
        totalLabel.text = ":"
        
    }
    
    @IBAction func calculateMeter(_ sender: UIButton) {
        
        let meter = Int(meterTextField.text!)
        if meter != nil &&  meter! <= 30{
            onetothirtyLabel.text = ":"+String(meter! * 35)
            totalLabel.text = ":"+String(meter! * 35)
        }
        else if meter! <= 50 {
            //var total:Int = 0
            var total1:Int = 0
            var total2:Int = 0
            var firstRange:Int = 30
            let meter = Int(meterTextField.text!)
            total1 = firstRange * 35
            let meter1 = meter! - firstRange
            total2 = meter1 * 50
            
            onetothirtyLabel.text = ":"+String(total1)
            thirtyonetofiftyLabel.text = ":"+String(total2)
             totalLabel.text = ":"+String(total1 + total2)
            
            
//
//
//            if (meter! >= 1 && meter! <= 30){
//                total1 = meter! * 35
//                onetothirtyLabel.text = ":"+String(total1)
//            }
//             if(meter! >= 31 && meter! <= 50){
//                total2  = meter! * 50
//                thirtyonetofiftyLabel.text = ":"+String(total2)
//            }
//            total = total1 + total2
//            totalLabel.text = ":"+String(total)
            
            
        }
        else if meter! <= 75 {
            
            var total1:Int = 0
            var total2:Int = 0
            var total3:Int = 0
            var firstRange:Int = 30
            var secondRange:Int = 20
            let meter = Int(meterTextField.text!)
            total1 = firstRange * 35
            total2 = secondRange * 50
            let meter1 = meter! - (firstRange + secondRange)
            total3 = meter1 * 70
            onetothirtyLabel.text = ":"+String(total1)
            thirtyonetofiftyLabel.text = ":"+String(total2)
            fiftyonetoseventyfiveLabel.text = ":"+String(total3)
            totalLabel.text = ":"+String(total1 + total2+total3)
        }
        else if meter! <=   100{
            var total1:Int = 0
            var total2:Int = 0
            var total3:Int = 0
             var total4:Int = 0
            var firstRange:Int = 30
            var secondRange:Int = 20
            var thirdRange:Int = 25
            let meter = Int(meterTextField.text!)
            total1 = firstRange * 35
            total2 = secondRange * 50
            total3 = thirdRange * 70
            let meter1 = meter! - (firstRange + secondRange + thirdRange)
            total4 = meter1 * 90
            onetothirtyLabel.text = ":"+String(total1)
            thirtyonetofiftyLabel.text = ":"+String(total2)
            fiftyonetoseventyfiveLabel.text = ":"+String(total3)
            seventysixtohundredLabel.text = ":"+String(total4)
            
            totalLabel.text = ":"+String(total1 + total2+total3+total4)
            
        }
        else if meter! <=   150{
            var total1:Int = 0
            var total2:Int = 0
            var total3:Int = 0
            var total4:Int = 0
            var total5:Int = 0
            var firstRange:Int = 30
            var secondRange:Int = 20
            var thirdRange:Int = 25
            var fourthRange:Int = 25
            let meter = Int(meterTextField.text!)
            total1 = firstRange * 35
            total2 = secondRange * 50
            total3 = thirdRange * 70
            total4 = fourthRange * 90
            let meter1 = meter! - (firstRange + secondRange + thirdRange+fourthRange)
            total5 = meter1 * 110
            onetothirtyLabel.text = ":"+String(total1)
            thirtyonetofiftyLabel.text = ":"+String(total2)
            fiftyonetoseventyfiveLabel.text = ":"+String(total3)
            seventysixtohundredLabel.text = ":"+String(total4)
            hundredonetohundredfiftyLabel.text = ":"+String(total5)
            totalLabel.text = ":"+String(total1 + total2+total3+total4+total5)
            
        }
        else if meter! <=   200{
            var total1:Int = 0
            var total2:Int = 0
            var total3:Int = 0
            var total4:Int = 0
            var total5:Int = 0
            var total6:Int = 0
            var firstRange:Int = 30
            var secondRange:Int = 20
            var thirdRange:Int = 25
            var fourthRange:Int = 25
            var  fifthRange:Int = 50
            let meter = Int(meterTextField.text!)
            total1 = firstRange * 35
            total2 = secondRange * 50
            total3 = thirdRange * 70
            total4 = fourthRange * 90
            total5 = fifthRange * 110
            let meter1 = meter! - (firstRange + secondRange + thirdRange+fourthRange+fifthRange)
            total6 = meter1 * 120
            onetothirtyLabel.text = ":"+String(total1)
            thirtyonetofiftyLabel.text = ":"+String(total2)
            fiftyonetoseventyfiveLabel.text = ":"+String(total3)
            seventysixtohundredLabel.text = ":"+String(total4)
            hundredonetohundredfiftyLabel.text = ":"+String(total5)
            hundredfiftyonetotwohundredLabel.text = ":"+String(total6)
            totalLabel.text = ":"+String(total1 + total2+total3+total4+total5+total6)
            
        }
        else if meter! >=  201{
            var total1:Int = 0
            var total2:Int = 0
            var total3:Int = 0
            var total4:Int = 0
            var total5:Int = 0
            var total6:Int = 0
            var total7:Int = 0
            var firstRange:Int = 30
            var secondRange:Int = 20
            var thirdRange:Int = 25
            var fourthRange:Int = 25
            var  fifthRange:Int = 50
            var  sixthRange:Int = 50
            let meter = Int(meterTextField.text!)
            total1 = firstRange * 35
            total2 = secondRange * 50
            total3 = thirdRange * 70
            total4 = fourthRange * 90
            total5 = fifthRange * 110
            total6 = sixthRange * 120
            let meter1 = meter! - (firstRange + secondRange + thirdRange+fourthRange+fifthRange+sixthRange)
            total7 = meter1 * 125
            onetothirtyLabel.text = ":"+String(total1)
            thirtyonetofiftyLabel.text = ":"+String(total2)
            fiftyonetoseventyfiveLabel.text = ":"+String(total3)
            seventysixtohundredLabel.text = ":"+String(total4)
            hundredonetohundredfiftyLabel.text = ":"+String(total5)
            hundredfiftyonetotwohundredLabel.text = ":"+String(total6)
            uptoTwohundredoneLabel.text = ":"+String(total7)
            totalLabel.text = ":"+String(total1 + total2+total3+total4+total5+total6+total7)
            
        }
        
        
        
        

        
    }
    

}

