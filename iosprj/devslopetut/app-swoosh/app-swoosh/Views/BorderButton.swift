//
//  BorderButton.swift
//  app-swoosh
//
//  Created by Khu Khu on 8/21/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.white.cgColor
    }

}
