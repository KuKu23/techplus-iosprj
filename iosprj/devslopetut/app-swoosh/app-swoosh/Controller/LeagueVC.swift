//
//  LeagueVC.swift
//  app-swoosh
//
//  Created by Khu Khu on 9/10/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class LeagueVC: UIViewController {

    var player:Player!
    
    
    @IBOutlet weak var nextBtn: BorderButton!
    
    @IBAction func onNextTapped(_ sender: Any) {
        performSegue(withIdentifier: "SkillVCSegue", sender: self)
    }
    
    
    @IBAction func onMensTapped(_ sender: Any) {
        selectLeage(leagueType: "mens")
        
    }
    
    
    @IBAction func onWomensTapped(_ sender: Any) {
        selectLeage(leagueType: "womens")
    }
    
    
    @IBAction func onCoedTapped(_ sender: Any) {
       selectLeage(leagueType: "coed")
    }
    
    func selectLeage(leagueType:String){
        player.desiredLeague = leagueType
        nextBtn.isEnabled = true
    }
    
    override func prepare(for segue:UIStoryboardSegue,sender:Any?){
        if let skillVC =  segue.destination  as? SkillVC{
            skillVC.player = player
        
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        player = Player()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
