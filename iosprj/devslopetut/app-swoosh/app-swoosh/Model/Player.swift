//
//  Player.swift
//  app-swoosh
//
//  Created by Khu Khu on 9/10/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import Foundation

struct Player {
    var desiredLeague:String!
    var selectedSkillLevel:String!
}
