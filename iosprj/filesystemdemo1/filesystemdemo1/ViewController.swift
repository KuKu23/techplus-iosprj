//
//  ViewController.swift
//  filesystemdemo1
//
//  Created by Khu Khu on 9/15/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //getInfo()
        deletefile()
    }
    
    func deletefile(){
        let fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("fileA.txt")
        do{
         try FileManager.default.removeItem(at: fileURL!)
            
            print("Deleted file")
        }catch let error{
            print(error.localizedDescription)
        }
    }
    
    func getInfo(){
        //FileManager
        let fm = FileManager()
        let docurl = fm.urls(for: .documentDirectory, in:.userDomainMask).first
        
        print(docurl)
        
        let cacheurl = fm.urls(for: .cachesDirectory, in: .userDomainMask).first?.appendingPathComponent("cachefile.txt")
        print(cacheurl)
        
        let tempurl = fm.temporaryDirectory.appendingPathComponent("mytext.txt")
        print(tempurl)
        
        //Write to the location
        if let fileUrl = docurl?.appendingPathComponent("fileA.txt"){
        
            do{
            try "Sunday".write(to: fileUrl, atomically: true, encoding: .utf8)
            try "Tempdata".write(to: tempurl, atomically: true, encoding: .utf8)
            try "Cachedata".write(to: cacheurl!, atomically: true, encoding: .utf32)
            }catch let error{
                print(error.localizedDescription)
            }
            
        }
    }


}

