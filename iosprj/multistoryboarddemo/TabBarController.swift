//
//  TabBarController.swift
//  multistoryboarddemo
//
//  Created by Khu Khu on 8/4/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
 
        let exerciseSB = UIStoryboard(name: "Exercise", bundle: nil)
        let exerciseVC = exerciseSB.instantiateViewController(withIdentifier: "exercise")
        
        let foodSB = UIStoryboard(name: "Food", bundle: nil)
        let foodVC = foodSB.instantiateViewController(withIdentifier: "foodvc")
        
        self.viewControllers = [exerciseVC,foodVC]
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
