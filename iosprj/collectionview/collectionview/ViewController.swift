//
//  ViewController.swift
//  collectionview
//
//  Created by Khu Khu on 8/4/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    var books = [Book]()
    
    var itemPerRow:CGFloat = 2
    var itemWidth:CGFloat = 0
    var itemHeight:CGFloat = 0
    var horizontalGap:CGFloat = 10
    var verticalGap:CGFloat = 10
    
    func getBooks(){
        let book1 = Book()
        let book2 = Book()
        books.append(book1)
        books.append(book2)
    }
    
    @IBOutlet weak var collectionView:UICollectionView!
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return books.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let currentBook = books[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath)
        cell.backgroundColor = UIColor.green
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.section,indexPath.row)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return horizontalGap
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return verticalGap
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: itemWidth, height: itemHeight)
        return size
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getBooks()
        collectionView.reloadData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.itemWidth = (collectionView.frame.size.width - horizontalGap * (itemPerRow-1))/itemPerRow
        self.itemHeight = (view.frame.size.height-20-verticalGap*2)/3
        collectionView.delegate = self
        collectionView.reloadData()
    }


}

