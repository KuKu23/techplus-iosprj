import UIKit


extension Int{
    func getString()->String{
        return String(self)
    }
    var string:String{
        return String(self)
    }
}

var number:Int = 123
var str:String = String(number)
print(str)

var str2:String = number.getString()
var str3:String = number.string
var str4:String = 456.string

