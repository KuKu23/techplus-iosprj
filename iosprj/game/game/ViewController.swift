//
//  ViewController.swift
//  game
//
//  Created by Khu Khu on 6/30/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let actor = UIImageView()
    let charWidth:CGFloat = 50
    let charHeight:CGFloat = 90
    var positionX:CGFloat = 20
    var positionY:CGFloat = 200
    var deltaX:CGFloat = 16
    var direction:CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        move()
    }
    
    func move(){
        Timer.scheduledTimer(withTimeInterval: 0.7, repeats: true) { (t) in
            if self.positionX>=self.view.frame.size.width{
                self.direction = -1
                self.positionY += 30
                self.actor.transform = CGAffineTransform(scaleX: -1, y: 1)
            }else if self.positionX<=0{
                self.direction = 1
                self.positionY += 30
                self.actor.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }
            self.positionX += (self.deltaX * self.direction)
            self.actor.frame.origin.x = self.positionX
            self.actor.frame.origin.y = self.positionY
            
        }
    }
    
    
    func setup(){
        
        actor.frame = CGRect(x: positionX, y: positionY, width: charWidth, height: charHeight)
        view.addSubview(actor)
        var images = [UIImage]()
        for i in 1...8{
            let image = UIImage(named:"pic\(i).png")!
            images.append(image)
        }
        actor.animationImages = images
        actor.startAnimating()
        
    }


}

