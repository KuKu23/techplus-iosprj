import UIKit

//OOP-
//Var + Function->Structural Programming

class Car{//Template to create something,instance Object
    private var _id:Int
    var id:Int{//computed properties,NOT Actual var
        return _id
    }
    public var material:String
    var numSeat:Int? //set if default value is appropriate
    var paintColor:String
    
    private var _speed:Float
    var speed:Float{
        return _speed
    }
    var oilLevel:Float
    private var status:Int = 0
    //1.Must Initialise properties
    //constructor-init
    init(id:Int){//set default in definition or in init or not required for optional
        self._id = id
        self.numSeat = 4
        self.material = "CastIron"
        self.paintColor = "White"
        self._speed = 0.0
        self.oilLevel = 0.0
        
    }
    //Methods
    func ignite()  {
        status = 1
        print("Car ignite...")
    }
    
    func drive(speed:Float){
        print("Driving.....")
        if status == 0{
            ignite()
        }
        while _speed<speed{
            _speed += 5
            print("Speeding up \(_speed)...")
        }
        
        while _speed>speed{
            _speed -= 5
            print("Slowdown \(_speed)...")
        }
        _speed = speed
        print("Drive at \(self.speed)...")
    }
    
    func getStatus()->String{
        //0-Stop,1-ignite,2-speedup,3-moving
        switch status {
        case 0:return "Stopped"
        case 1:return "Ignite"
        case 2:return "SpeedUp"
        case 3:return "Moving"
        default:return "Unknown"
        }
    }
    
    func stop(){
        self.drive(speed: 0)
        print("Car stopped")
    }
}

class Jeep:Car{//only one parent can be inheritance
    var durability:Int
    
    override init(id:Int){
        //override=> when to override parent class method,name same,parameter same
        durability = 10//first initialise its own properties then
        super.init(id: id)
        self.paintColor = "Green"
        
        
    }
    
    
}
var car1 = Car(id: 1)
var car2 = Car(id: 2)
var jeep1 = Jeep(id: 3)

jeep1.drive(speed: 10)
print("Jeep Color",jeep1.paintColor)
print("Jeep Speed:\(jeep1.speed)")

print("Car1ID:",car1.id)
print(car1.speed)
print(car2.speed)

car1.drive(speed: 120)
print("Car2 speed:",car2.speed)
car1.drive(speed: 20)
car1.stop()




