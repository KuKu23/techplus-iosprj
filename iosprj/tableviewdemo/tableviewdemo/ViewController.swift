//
//  ViewController.swift
//  tableviewdemo
//
//  Created by Khu Khu on 7/7/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    @IBOutlet weak var table: UITableView!
    
    
    
    
    

    var coffeetypes = ["Expresso","Capuccino","Macciato","Macha","Latte"]
    var cupImageStrings = ["cup1.png","cup2.png","cup3.png","cup4.png","cup5.png"]
    
    //tableview.reloadData
    
    //1,2,3
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.size.height/6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //prototype cell
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! CoffeeTableViewCell//type casting
        //coz of class assignment already
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        cell.coffeeLabel.text = coffeetypes[indexPath.row]
        cell.cupImageView.image = UIImage(named: cupImageStrings[indexPath.row])
        
       
        return cell
    }
    
    //MARK:TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let currentCoffee = coffeetypes[indexPath.row]
        let coffeeImage = cupImageStrings[indexPath.row]
        let coffee = (currentCoffee,coffeeImage)//tuple
        performSegue(withIdentifier: "detailsegue", sender: coffee)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier{
        case "detailsegue":let coffee = sender as!(String,String)//type casting
            let destVC = segue.destination as? DetailViewController //typecasting
            destVC?.coffee = coffee //Carry on
        default:()
        }
        
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }


}

