//
//  DetailViewController.swift
//  tableviewdemo
//
//  Created by Khu Khu on 7/20/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    
    @IBOutlet weak var cupImageView: UIImageView!
    
    
    @IBOutlet weak var coffeeNameLabel: UILabel!
    
    var coffee:(String,String)!//assigned from caller vc
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("In detail VC")
        print(coffee.0,coffee.1)//coffee data is already loaded since prepare for segue method in ViewController
        
        coffeeNameLabel.text = coffee.0
        cupImageView.image = UIImage(named:coffee.1)
        cupImageView.contentMode = .scaleAspectFit
        
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
