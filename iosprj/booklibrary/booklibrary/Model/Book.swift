//
//  Book.swift
//  booklibrary
//
//  Created by Khu Khu on 9/15/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import Foundation

class Book{
    var id:String
    var title:String
    var author:String
    var desc:String
    
    init(id:String,title:String,author:String,desc:String) {
        self.id = id
        self.title = title
        self.author = author
        self.desc = desc
    }
    
    func save()-> String?{
        let docurl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let numsec = Date.timeIntervalSinceReferenceDate
        let filename = bookprefix + "\(numsec)" + ".txt"
        let fileurl = docurl?.appendingPathComponent(filename)
        
        do{
            try "\(id),\(title),\(author),\(desc)".write(to: fileurl!, atomically: true, encoding: .utf8)
        }catch let error{
            print(error.localizedDescription)
        }
        
        return nil
        
    }
}
