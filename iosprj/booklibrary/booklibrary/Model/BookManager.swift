//
//  BookManager.swift
//  booklibrary
//
//  Created by Khu Khu on 9/15/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import Foundation

class BookManager{
    var books = [String]()
    
    
    func loadBook()->[String]{
        
        let docurl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        
        let dbfile = docurl?.appendingPathComponent(DBFileName)
        do{
            let bookNameString = try String.init(contentsOf:dbfile!)
            books = bookNameString.components(separatedBy: ",")
            return books
            
        }catch let error{
            print("book db can't find,return default")
        }
        return books
    }
    
    func addNewBook(bookname:String){
        books.append(bookname)
        let docurl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let dbfile = docurl?.appendingPathComponent(DBFileName)
        do{
            let booksString = books.joined(separator: ",")
            try booksString.write(to: dbfile!, atomically: true, encoding: .utf8)
             
            
        }catch let error{
            print("can't save book")
        }
    }
}
