//
//  ViewController.swift
//  Guessprj
//
//  Created by Khu Khu on 6/16/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var meaningLabel: UILabel!
    @IBOutlet weak var alphabetLabel: UILabel!
    
     @IBOutlet weak var btnA: UIButton!
     @IBOutlet weak var btnB: UIButton!
     @IBOutlet weak var btnC: UIButton!
     @IBOutlet weak var btnD: UIButton!
     @IBOutlet weak var btnE: UIButton!
    
    let randomAlphabets = ["A","B","C","D","E"]
    let dict = ["A":"Apple","B":"Ballon","C":"Cat","D":"Dragon","E":"Elephant"]
    
    @IBAction func sayIt(_ sender: UIButton) {
        
        let randNo = Int.random(in: 0..<randomAlphabets.count)
        let theAlphabet = randomAlphabets[randNo]
        let theMeaning = dict[theAlphabet]
        
        alphabetLabel.text = theAlphabet
        meaningLabel.text = theMeaning
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    btnA.layer.cornerRadius = btnA.frame.size.width/2
    btnB.layer.cornerRadius = btnB.frame.size.width/2
    btnC.layer.cornerRadius = btnC.frame.size.width/2
    btnD.layer.cornerRadius = btnD.frame.size.width/2
    btnE.layer.cornerRadius = btnE.frame.size.width/2
        
    btnA.layoutIfNeeded()
    btnB.layoutIfNeeded()
    btnC.layoutIfNeeded()
    btnD.layoutIfNeeded()
    btnE.layoutIfNeeded()
        
        
        
        
        
    }


}

