var num1:Int = 0

//?is indicate the type is optional
var num2:Int?

//print(num1)
//print(num2)
//num1 = 1
//num2 = 4
//print(num1)
//print(num2)

//check
//check before use optional
//use to '!' to convert optional to non optional


num2 = nil
//!is used when value is 100% presented in that var


var myArray:[Int]? = [100,200,300,400,500]

myArray?.removeAll()
myArray = nil

//!=> forced unwrapping
//var num3:Int = num2!

//if num2 != nil{
    //var r2 = num2! + 5
//}

//if let is used to unwrap normal, safe unwrapping
if let num2 = num2{
    //this following code will execute only after num2 has value
    print(num2)
}



//arraylike but its element has in pair(K:V)
var myDict: [String:String] = ["A":"Apple","B":"Baloon","C":"Cat","D":"Dog","E":"Elephant"]

var numDict:[Int:String] = [1:"One",2:"Two",3:"Three"]

var fakeArrayLikeDict:[Int:Int] = [0:100,1:200,3:300,4:400]

var emptyDict:[String:String] = [String:String]()

print(myDict)
myDict["F"] = "Fish"
print(myDict)
myDict["A"] = nil

var personDict:[String:Any] = ["name":"Tim Cook","age":65.7,"degrees":["MBA","BEng"]]

var myNums = [31,32,33,34,35,36,37]

//while
//1.index must be initialise, or at know state
//2.loop statement must exist in certain condition
//3.index must be increase or decrease inside loop

var i = 0
while i<myNums.count{ //while expression{code}
    print(myNums[i])
    i+=1//i++ Sorry,i--,++i,--i
}
//while is Not For Dictiory
//for each loop
print("----------")
for e in myNums{
    print(e)
}


for (k,v) in myDict{
    print(k)
    print(v)
}

for i in 0..<myNums.count{
    print("=>",myNums[i])
    myNums[i] = myNums[i] * 10
}

var j = 0
while j<myNums.count{
    myNums[j] = myNums[j] * 10
    print(myNums[j])
    j += 2
}

print("-------------")



