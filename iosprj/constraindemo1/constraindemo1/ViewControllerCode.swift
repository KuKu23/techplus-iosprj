//
//  ViewControllerCode.swift
//  constraindemo1
//
//  Created by Khu Khu on 7/21/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewControllerCode: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //Mark
        let imageView = UIImageView()
        imageView.image = UIImage(named: "photo1.jpg")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        //1.let be child view
        
        view.addSubview(imageView)
       //2.Disable automatic constrain
        imageView.translatesAutoresizingMaskIntoConstraints = false
        //3.set constrain(follow 4 constrain rule)
     imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        
        //Mark:Respective Height Constarint
        let coverView = UIView()
        coverView.backgroundColor = UIColor.red
        view.addSubview(coverView)

    coverView.translatesAutoresizingMaskIntoConstraints = false
        
    coverView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
    coverView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
    coverView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        coverView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.25,constant:0).isActive = true
        
        //Mark:Center Constraint Apply
        let centerView = UIView()
        centerView.backgroundColor = UIColor.white
        view.addSubview(centerView)
        centerView.translatesAutoresizingMaskIntoConstraints = false
//        centerView.widthAnchor.constraint(equalToConstant: 100).isActive = true
//        centerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        centerView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.25, constant: 0).isActive = true
        centerView.heightAnchor.constraint(equalTo: centerView.widthAnchor, multiplier: 1.0, constant: 0).isActive = true
        centerView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        centerView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
        
        //Mark:Reference to other UI constraint
        let topView = UIView()
        topView.backgroundColor = UIColor.red
        view.addSubview(topView)
        topView.translatesAutoresizingMaskIntoConstraints = false
        
        topView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        topView.leadingAnchor.constraint(equalTo: centerView.leadingAnchor, constant: 0).isActive = true
        topView.widthAnchor.constraint(equalTo: centerView.widthAnchor, multiplier: 0.5).isActive = true
         topView.heightAnchor.constraint(equalTo: centerView.heightAnchor, multiplier: 0.5).isActive = true
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
