//
//  ViewController.swift
//  iboutletibaciondemo1
//
//  Created by Khu Khu on 6/9/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var subtitleLabel:UILabel!
    
    @IBOutlet weak var btn1:UIButton!
    
    @IBAction func changeTitle(_ sender: Any) {
        titleLabel.text = "Yes we are"
        titleLabel.textColor = UIColor.orange
    }
    
    @IBAction func updateText(_sender:UIButton){
        subtitleLabel.text = "In December 2019"
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        titleLabel.text = "Professional iOS Developer Course"
        
        subtitleLabel.text = "User Interface Demo"
        
        titleLabel.frame.size.width = titleLabel.intrinsicContentSize.width
        
        btn1.layer.borderColor = UIColor.gray.cgColor
        btn1.layer.borderWidth = 2
        btn1.layer.cornerRadius = 7
        
       
    }


}

