//
//  ViewController.swift
//  mapviewdemo
//
//  Created by Khu Khu on 9/8/19.
//  Copyright © 2019 Khu Khu. All rights reserved.
//

import UIKit
import MapKit



class ViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet var addressTextField: UITextField!
    
    @IBOutlet var resultTextView: UITextView!
    
    @IBOutlet weak var map:MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressTextField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        geocode(textField.text ?? "")
        return true
    }
    
    func geocode(_ address:String){
 CLGeocoder().geocodeAddressString(address){(placemarks,error) in
    if error != nil{print(error?.localizedDescription);return;}
    if let places = placemarks,places.count>0{
        if let location = places[0].location{
            self.map.showOnMap(location.coordinate.latitude, location.coordinate.longitude,0.0002)
        }
        if let firstInterest = places[0].areasOfInterest,firstInterest.count>0{
            
            var ai = ""
            for s in firstInterest{
                ai += s + ""
            }
            self.resultTextView.text = ai
        }
    }
            
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        map.showOnMap(16.857095, 96.1546, 0.002)
        dropPin(16.857095, 96.1546)
    }
    
    func dropPin(_ lat:CLLocationDegrees, _ long:CLLocationDegrees){
        var pins = [MKPointAnnotation]()
        for i in 0...10{
            let pin = MKPointAnnotation()
            pin.coordinate = CLLocationCoordinate2D(latitude: lat , longitude: long + Double((i/2)))
            pins.append(pin)
        }
       
        
       map.addAnnotations(pins)
    }

    

}

extension MKMapView{
    func showOnMap(_ lat:CLLocationDegrees,_ long:CLLocationDegrees,_ deltaDeg:CLLocationDegrees){
    
    let location = CLLocationCoordinate2D(latitude:lat,longitude:long)
    let span = MKCoordinateSpan(latitudeDelta: deltaDeg, longitudeDelta: deltaDeg)
    
    let region = MKCoordinateRegion(center: location, span: span)
    
    self.setRegion(region, animated: true)
    
}
}
