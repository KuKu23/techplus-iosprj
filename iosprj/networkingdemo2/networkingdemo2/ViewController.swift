//
//  ViewController.swift
//  networkingdemo2
//
//  Created by Admin on 16/11/2019.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit


struct Post:Codable{
    var id:Int
    var title:String
    var author:String
}



class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        read()
        
        
    }
    let baseURL = "http://localhost:3000"
    func read(){
        let session = URLSession(configuration: URLSessionConfiguration.default)
        var request = URLRequest(url:URL(string:baseURL + "/posts")!)
        request.httpMethod = "GET"
        let task = session.dataTask(with:request){
            (data,response,error)in
            if (error==nil && data != nil){
                let decoder = JSONDecoder()
                let post = try! decoder.decode(Post.self, from: data!)
                print(post.title,post.author)
            }
        }
    }
    
    func write(){
        let session = URLSession(configuration: URLSessionConfiguration.default)
        var request = URLRequest(url:URL(string:baseURL + "/posts")!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = ["Content-":"application/x-www-from-urlencoded"]
        
        let newPost = Post(id:4,title:"New Post",author: "Min")
        let encoder = JSONEncoder()
        let data = try! encoder.encode(newPost)
        request.httpBody = data
        
        let task = session.dataTask(with:request){
        (data,response,error)in
            if (error==nil && data != nil){
                print("Successful")
            }
        
    }


}
}
