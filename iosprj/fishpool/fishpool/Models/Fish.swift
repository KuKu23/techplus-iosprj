import UIKit


class Fish {
    private var _id:Int
    var color:UIColor
    var id:Int {
        return _id
    }
    private var age:Float
    private var lifeSpan:Float
    private var speed:Float = 0
    var visibility:Float = 100
    var status:Int //0-sleep, 1-swimming forward, 2-rotate left, 4-rotate right, 8-Hungry ,16------
    var body:UIView
    init(id:Int){
        self._id = id
        self.color = UIColor.white
        self.age = 1 //in day
        self.status = 0
        
        self.visibility = 100
        self.lifeSpan = 30 //30 sec
        
        self.body = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 80))
        let bodyImage = UIImageView(image: UIImage(named:"fish"))
        bodyImage.contentMode = .scaleAspectFit
        bodyImage.frame = self.body.frame
        self.body.addSubview(bodyImage)
        
        
    }
    func place(x:CGFloat,y:CGFloat){
        self.body.frame.origin.x = x
        self.body.frame.origin.y = y
    }
    private func getWhatToDo( )->Int {
        return 0
    }
    func   getAge()->Float {  //Getter
        return self.age
    }
    func swim() {
        let x:CGFloat = 600
        let y:CGFloat = 200
        UIView.animate(withDuration: 2) {
            self.body.frame.origin.x = x
            self.body.frame.origin.y = y
        }
    }
    func swim(x: CGFloat, y:CGFloat ){
        
    }
    func swim(dx:CGFloat, dy:CGFloat){
        
    }
    func findFood(){
        
    }
}
