
import UIKit
import MobileCoreServices

class ViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    let imageMediaType = kUTTypeImage as String
    let movieMediaType = kUTTypeMovie as String

    @IBOutlet weak var profileImageView: UIImageView!
    var imagePicker = UIImagePickerController()
    
    @IBAction func addPhoto(_ sender: UIButton) {
            //pickFromLibrary()
        pickFromCamera()
    }
    func pickFromCamera() {
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.cameraFlashMode = .on
        imagePicker.cameraDevice = .rear
        imagePicker.mediaTypes = [movieMediaType,imageMediaType]
        present(imagePicker, animated: true, completion: nil)
    }
    func pickFromLibrary() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       imagePicker.delegate = self //**********
    }
    override func viewDidAppear(_ animated: Bool) {
        
        var docURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let fileURL = docURL!.appendingPathComponent( "profile.png")
        do {
              let data = try   Data.init(contentsOf: fileURL )
             let image = UIImage(data: data)
            profileImageView.image =  image
        } catch {
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        
        switch (info[.mediaType] as! String) {
        case movieMediaType:let tempURL = info[.mediaURL]
            print(tempURL)
        case imageMediaType:let image =  info[UIImagePickerController.InfoKey.editedImage] as! UIImage //got data
        profileImageView.image = image
        
        var docURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        
        let fileURL = docURL!.appendingPathComponent( "profile.png")
        let imageData  = image.pngData()
        do {
            try imageData!.write(to: fileURL)
        } catch {
            
            }
        default:()
        
        }
        
    }

}

